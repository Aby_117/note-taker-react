import { useState, useEffect } from 'react'
import './App.css'
import TextEditor from './components/TextEditor'
import Sidebar from './components/Sidebar'
import {nanoid} from "nanoid"
import Split from 'react-split'
import _ from 'lodash';

function App() {

  //Lazy state getting local storage reduces processing power needed to load content
  const [notes, setNotes] = useState(() => JSON.parse(localStorage.getItem("notes"))  || [])
  const updateNoteDebounced = _.debounce(updateNote, 300); // Adjust the delay as needed
  const [currentNoteId, setCurrentNoteId] = useState(
    (notes[0] && notes[0].id) || ""
  )

  const findCurrentNote = () => notes.find(note => {
    return currentNoteId === note.id
  }) || notes[0]

  useEffect(() => {
    localStorage.setItem("notes", JSON.stringify(notes))
  }, [notes])

  function createNewNote() {
    const newNote = {
      id: nanoid(),
      body: "# type your markdown note's title here"
    }
    setNotes(prevNotes => [newNote, ...prevNotes])
    setCurrentNoteId(newNote.id)
  }

  function deleteNote(event, noteID) {
    event.stopPropagation()
    setNotes(oldNotes => oldNotes.filter(oldNote => oldNote.id !== noteID))
  }

  //Updates the text inside the note
  //Brings the note up in the sidebar
  function updateNote(text) {
    setNotes(oldNotes => {
      const newArray = []
      for (let i=0; oldNotes.length > i; i++) {
        const oldNote = oldNotes[i]
        if (oldNote.id === currentNoteId) {
          newArray.unshift({...oldNote, body: text})
        } else {
          newArray.push(oldNote)
        }
      }
      return newArray
    })
  }

  return (
    <main>
      {
        notes.length > 0
        ? 
        <Split
          sizes={[30, 70]}
          direction="horizontal"
          className="split"
        >
          <Sidebar
            notes={notes}
            deleteNote={deleteNote}
            currentNote={findCurrentNote()}
            setCurrentNoteId={setCurrentNoteId}
            newNote={createNewNote}
          /> 

          {
            currentNoteId &&
            notes.length > 0 &&
            <TextEditor
              key={currentNoteId}
              currentNote={findCurrentNote()}
              updateNote={updateNoteDebounced}
            />
          }
        </Split>
        :
        <div className='no-notes'>
          <h1>You have no notes</h1>
          <button
            className='first-note'
            onClick={createNewNote}
          >
            Create one now
          </button>
        </div>
      }
    </main>
  )
}

export default App
