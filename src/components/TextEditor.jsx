import { useState, useRef } from 'react'
import React from "react";
import ReactQuill from 'react-quill';
import 'quill/dist/quill.snow.css'; // Import Quill styles
import 'react-quill/dist/quill.bubble.css'; // Import React-Quill styles (optional)

import './../App.css'


function TextEditor({currentNote, updateNote}) {


  const modules = {
    toolbar: [
      [{ header: [1, 2, 3, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ align: [] }],  // Add this line for text alignment options (center, right, left, justify)
      [{ list: 'ordered' }, { list: 'bullet' }],
      ['link', 'image'],
      ['clean'],
    ],
  };

  const formats = [
    'header',
    'bold', 'italic', 'underline', 'strike',
    'align',  // Add this line for text alignment options (center, right, left, justify)
    'list', 'bullet',
    'link', 'image',
  ];

  return ( 
    <section className='pane editor'>
        <ReactQuill 
          modules={modules}
          formats={formats}
          theme='snow'
          value={currentNote.body}
          onChange={updateNote}
        />
    </section>
  ) 
}

export default TextEditor;
