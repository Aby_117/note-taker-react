import { useState } from 'react'
import './../App.css'

export default function Sidebar(props) {

    //Finds the tab title
    function findTitle(body) {
        const isBodyText = body.split(/<\/?\w+[^>]*>/g)
        for (let i=1; i < isBodyText.length; i++) {
            if (isBodyText[i].length > 4) {
                return isBodyText[i]
            } 
        }
    }

    const noteElements = props.notes.map((note, index) => 
        <div key={note.id}>
            {note.id === props.currentNote.id}
            <div
                className={`title ${
                    note.id === props.currentNote.id ? "selected-note" : ""
                }`}
                onClick={() => props.setCurrentNoteId(note.id)}
            >
                <h4 className="text-snippet">{findTitle(note.body) || `Empty ${index + 1}`}</h4>
                <button 
                    className="delete-btn"
                    onClick={(event) => props.deleteNote(event, note.id)}
                >
                    <i className="gg-trash trash-icon"></i>
                </button>
            </div>
        </div>
    )

    return (
        <section className='pane sidebar'>
            <div className='sidebar--header'>
                <h3>Notes</h3>
                <button className='new-note' onClick={props.newNote}>+</button>
            </div>
            {noteElements}
        </section>
    )
}