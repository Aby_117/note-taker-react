# Simple Browser Note Taker using React + Vite

## Description
It adds notes and stores there content in the browser's local storage. Notes can be edited, titled, added, and deleted all through the UI.

## Usage
In the Project Directory Run: 
`npm install`
then
`npm run dev`

## Roadmap
The GUI is elementary as is using the app:
1. Create a new note
2. Add text to it
3. Close the browser
4. Open the browser to check on the note
5. Edit or delete any of the notes

## Dependencies
- **react-split**: Splits the screen into two sections
- **react-quill**: Rich text editor integrated into the project

## Authors and acknowledgment
Aby 

## License
MIT License

## Project status
Complete